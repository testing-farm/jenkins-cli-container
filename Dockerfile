FROM anapsix/alpine-java

COPY jenkins-cli.jar /jenkins-cli.jar
COPY entrypoint.sh /entrypoint.sh

CMD ["/entrypoint.sh"]
