Container image bundling jenkins-cli for interacting with Jenkins
==

Use this container to easily run jenkins-cli wherever you need.

```
docker run quay.io/testing-farm/jenkins-cli -version
```
